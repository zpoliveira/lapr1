package lapr1;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;

public class Lapr1 {

    //matriz principal, onde vao ser introduzidos numeros a serem tratados
    public final static double[][][] theMatrix = new double[100][100][100];

    public static void main(String[] args) throws FileNotFoundException {

        /*
        argumento[0] método a utilizar
        argumento[1] limiar para descartar critérios
        argumento[2] limiar RC
        argumento[3] fileIn
        argumento[4] fileOut
         */
        String argumentos[] = validacaoargumentos(args);

        if (argumentos[0].equals("1")) {
            //metodo de verificacao de erros no ficheiro
            ErrosLeitura.errosAhp(argumentos[3]);
            //cabecalho de criterios
            String[] crit = Leitura_txt.lerNumCriterios();
            //numero de criterios
            int nCrit = crit.length - 1;
            //cabecalho de alternativas
            String[] alt = Leitura_txt.lerNumAlternativas(nCrit);
            //numero de alternativas
            int nAlt = alt.length - 1;
            //criterios eliminados
            String[] critEliminados = new String[nCrit + 1];
            //metodo para verificaçao do limiar, retorna o novo numero de criterios validos
            nCrit = LIMIAR.verificaLimiar(argumentos[1], theMatrix, nCrit, crit, critEliminados);
            //menu para escolha de metodo aproximado ou exato
            int op = menuAHP();
            //metodos de calculo AHP
            MetodosComuns.calculoAhp(theMatrix, nCrit, nAlt, crit, alt, op, argumentos[2]);
            //metodo de output para consola e ficheiro
            Escrita_txt.escritaAhp(argumentos[4], crit, alt, nCrit, nAlt, critEliminados);
        } else {
            //metodo de verificacao de erros no ficheiro, retorna um string com os criterios
            String Crit = ErrosLeitura.errosTopsis(argumentos[3]);
            //cria vetor com os criterios
            String[] vecCrit = Crit.split("  ");
            //metodo que  vai buscar as alternativas ao ficheiro, converte em vetor
            String[] vecAlt = Leitura_txt.vetorAlternativasTopsis(argumentos[3], Crit);
            //nuemero de criterios que sao custo
            int custo = (int) theMatrix[0][0][0];
            //se o ficherio de entarada nao tiver pesos, introduz peso uniforme
            if (theMatrix[0][1][0] == 0) {
                System.out.printf("%n%S%n", "vectores de pesos em falta, utilização de peso uniforme (1/numero_de_criterios)");
                double peso = (double) 1 / vecCrit.length;

                pesoUni(peso, theMatrix, vecCrit.length);

            }
            //introducao de vetor e matriz na matriz princicpal
            Leitura_txt.leituraTopsis(vecCrit, vecAlt);
            //metodos de calculo TOPSIS
            MetodosComuns.calculoTOPSIS(theMatrix, vecAlt.length, vecCrit.length, custo);
            //metodo de output para ficheiro e consola
            Escrita_txt.escritaTopsis(argumentos[4], vecCrit, vecAlt);
        }

    }

    public static String[] validacaoargumentos(String[] args) {
        // java -jar nome_programa.jar [Options] nome_ficheiro_input.txt nome_ficheiro_output.txt
        /*
        -M 1 #Execução do Método AHP 
        -M 2 #Execução do Método TOPSIS 
        -L 0.15 #Limiar a utilizar no método AHP para descartar atributos irrelevantes 
        -S 0.20 #Limiar de consistência
         */
        String[] argumentos = new String[5];
        argumentos[1] = "0";
        argumentos[2] = "0.1";

        // primeiro validar o numero maximo e minimo de argumentos
        if (args.length > 8 || args.length < 3 || args.length == 7) {
            System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
            System.exit(0);
        } else {
            // validar o primeiro argumento. obrigatorio ser sempre -M
            if (!args[0].equals("-M")) {
                //System.out.println("Primeiro Argumento:"+args[0]);
                System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                System.exit(0);
            } else {
                // validar o segundo argumento. apenas duas opcoes possiveis.
                try {
                    int metodo = Integer.parseInt(args[1]);
                    if (metodo != 1 && metodo != 2) {
                        System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                        System.exit(0);
                    }
                    if (metodo == 1) {
                        argumentos[0] = "1";
                        //System.out.println("Metodo 1: AHP");
                        // validar as restantes possibilidades para metodo AHP
                        if (!args[2].equals("-L") && !args[2].equals("-S") && args.length != 4) {
                            System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                            System.exit(0);
                        }
                        if (args[2].equals("-L") || args[2].equals("-S")) {
                            //System.out.println("Primeira Personalizacao:"+args[2]);
                            if (args.length < 6) {
                                System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                                System.exit(0);
                            }
                            try {
                                double restricaoum = Double.parseDouble(args[3]);
                                if (restricaoum > 1 || restricaoum < 0) {
                                    System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                                    System.exit(0);
                                } else {
                                    if (args[2].equals("-L")) {
                                        argumentos[1] = args[3];
                                    } else {
                                        argumentos[2] = args[3];
                                    }
                                }

                            } catch (NumberFormatException e) {
                                System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                                System.exit(0);
                            }
                            // inicio da verificao da possibilidade de L e S 
                            if (args[4].equals("-L") || args[4].equals("-S")) {
                                //System.out.println("Segunda Personalizacao:"+args[4]);
                                if (args[4].equals(args[2]) || (args.length < 8)) {
                                    System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                                    System.exit(0);
                                }
                                try {
                                    double restricaodois = Double.parseDouble(args[5]);
                                    if (restricaodois > 1 || restricaodois < 0) {
                                        System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                                        System.exit(0);
                                    } else {
                                        if (args[4].equals("-L")) {
                                            argumentos[1] = args[5];
                                        } else {
                                            argumentos[2] = args[5];
                                        }
                                    }

                                } catch (NumberFormatException e) {
                                    System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                                    System.exit(0);
                                }
                            }
                        }
                    }
                    // fim da validacao do metodo AHP
                    if (metodo == 2) {
                        argumentos[0] = "2";
                        //System.out.println("Metodo 2: TOPSIS");
                        if (args.length != 4) {
                            System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                            System.exit(0);
                        }
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Número de argumentos inválidos. A aplicação deve ser invocada utilizando: java -jar Lapr1.jar [Options] nome_ficheiro_input.txt_output.txt");
                    System.exit(0);
                }
            }
            // prevenir outro metodo qualquer
            // fim do try para Metodo
            // validar os ficheiros
        }

        File ficheiroentrada = new File(args[args.length - 2]);
        if (ficheiroentrada.exists() && !ficheiroentrada.isDirectory()) {
            argumentos[3] = args[args.length - 2];
        } else {
            System.out.println("Ficheiro de Entrada não existe.");
            System.exit(0);
        }

        File ficheirosaida = new File(args[args.length - 1]);
        if (ficheirosaida.exists() || ficheirosaida.isDirectory()) {
            System.out.println("Ficheiro de Saida já existe.");
            System.exit(0);
        } else {
            argumentos[4] = args[args.length - 1];
        }
        return argumentos;
    }

    public static int menuAHP() {

        Scanner input = new Scanner(System.in);
        int op;
        do {
            System.out.println("\n" + "Escolha entre um método de cálculo:");
            System.out.println("\n" + "Opção(1) para Método Exato");
            System.out.println("Opção(2) para Método Aproximado");
            op = input.nextInt();

            if (op != 1 && op != 2) {
                System.out.println("\n" + "Opção inválida");
            }

        } while (op != 1 && op != 2);

        return op;
    }

    /**
     * introduz pesos uniformes na matriz
     *
     * @param peso - 1/ncrit
     * @param theMatrix - matriz princial
     * @param nCrit - numero de criterios
     */
    public static void pesoUni(double peso, double[][][] theMatrix, int nCrit) {

        for (int i = 0; i < nCrit; i++) {

            theMatrix[i][0][0] = peso;

        }
    }
}
