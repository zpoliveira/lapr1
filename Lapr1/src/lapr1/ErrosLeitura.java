package lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;
import static lapr1.Lapr1.theMatrix;

public class ErrosLeitura {

    /**
     * metodo de verificaçao de erros do ficheiro de entrada AHP
     *
     * @param fileName :nome do ficheiro de entrada
     * @throws FileNotFoundException
     */
    public static void errosAhp(String fileName) throws FileNotFoundException {
        //criar nome do ficheiro de erros com data e horario
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH'h'mm'm'ss's'");
        Date date = new Date();
        String errorFile = "erros_" + dateFormat.format(date);

        Scanner fInput = new Scanner(new File(fileName));
        String auxFile = "ficheiro_auxiliar";
        //ficheiro para onde vao ser passadas as linhas a ser tratadas, e ficheiro de erros
        Formatter outAuxFile = new Formatter(new File(auxFile)), outErros = new Formatter(new File(errorFile));
        //contador de linhas, e de erros
        int lineCount = 0, errorCount = 0;
        //Strings a serem comparadados nas validacoes
        String auxCrit = "mc_criterios";
        String auxMcp = "mcp_";
        //vai lhe ser atribuido o comprimento do cabecalho da primeira matriz de comparacao encontrada
        int auxline = 0;

        //correr pelo ficheiro de entrada
        while (fInput.hasNext()) {
            String linha = fInput.nextLine();
            lineCount++;
            //ignora linhas em branco
            while (fInput.hasNext() && linha.trim().length() == 0) {
                linha = fInput.nextLine();
                lineCount++;
            }
            //verifica se a primeira linha  com informação tem menos comprimento que o String mc_crterios, se vdd dá erro, e sai do programa
            if (linha.trim().length() <= auxCrit.length()) {
                //metodo para retorno de erros
                erros(linha, lineCount, "cabecalho de criterios nao encontrado no inicio do ficheiro, ou escrito incorretamente", outErros);
                errorCount++;
                outErros.close();
                outAuxFile.close();
                System.exit(0);
            } else {
                //verifica se a linha tem mais que dois criterios ou se tem o String mc_criterios no inicio
                //so arranca com 2 ou mais criterios
                if (linha.trim().length() >= auxCrit.length() && (linha.trim().split("  ").length <= 2 || !linha.trim().substring(0, auxCrit.length()).equalsIgnoreCase(auxCrit))) {
                    erros(linha, lineCount, "cabecalho de criterios nao encontrado no inicio do ficheiro,ou escrito incorretamente", outErros);
                    errorCount++;
                    outErros.close();
                    outAuxFile.close();
                    System.exit(0);
                } else {
                    //passa o cabecalho validado para o ficheiro axiliar
                    outAuxFile.format("%s", linha.trim());
                    //cria um vetor com o cabecalho de criterios
                    String[] vecCrit = linha.trim().split("  ");
                    linha = fInput.nextLine();
                    lineCount++;
                    /*
                    ciclo para correr pela matriz de criterios, segundo o numero de criterios em vecCrit;
                    menos 1 porque:
                    vecCrit = ["mc_criterios"][criterios]...[criterios]
                     */
                    for (int i = 0; i < vecCrit.length - 1; i++) {
                        //trocas todos os espacos por 1 espaco
                        linha = linha.trim().replaceAll("\\s+", " ");
                        //verifica se a linha da matriz tem o numero de elementos correto
                        if (linha.split(" ").length != vecCrit.length - 1) {
                            erros(linha, lineCount, "numero de elementos da matriz incorreto", outErros);
                            errorCount++;
                        } else {
                            //caso nuemro de elementos correto, cria um vetor com os numeros encontados na linha
                            String[] num = linha.split(" ");
                            //ciclo para correr pelo vetor de numeros
                            for (int j = 0; j < vecCrit.length - 1; j++) {
                                /*
                                verifica se os numeros sao validos, utiliza o metodo "auxiliar" para essa validacao;
                                 */
                                if (auxiliar(num[j], linha, lineCount, outErros) == false) {
                                    erros(linha, lineCount, "valor numerico invalido", outErros);
                                    errorCount++;
                                }
                            }
                            //manda linha de numeros para o ficheiro auxiliar
                            outAuxFile.format("%n%s", linha.trim());
                        }
                        if (fInput.hasNext()) {
                            linha = fInput.nextLine();
                            lineCount++;
                        }
                    }
                    /*
                    continua a leitura para procurar as matrizes de comparacao;
                    ignora linhas em branco novamente
                     */
                    while (fInput.hasNext() && linha.trim().length() == 0) {
                        linha = fInput.nextLine();
                        lineCount++;
                    }
                    /*
                    ciclo para limitar o numero de mats de comp consoante o numero de criterios estabelecido
                     */
                    for (int i = 1; i < vecCrit.length; i++) {
                        //Cria um string com "mcp_+nome do criterio"
                        String cabecalho = auxMcp + vecCrit[i].trim();
                        //verifica se a linha tem menor ou igual comprimento que o string cabecalho criado, retorna erro, e sai do programa
                        if (linha.trim().length() <= cabecalho.length()) {
                            erros(linha, lineCount, "cabecalho de matriz de comparacao nao encontrado ,ou escrito incorretamente", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);

                        } else {
                            /*
                            verifica se a linha tem pelo menos duas alternativas e se a descriçao tem no incio o string
                            cabecalho estabelecido anteriormente
                            retorna erro e sai do programa
                             */
                            if (linha.trim().length() >= cabecalho.length() && (linha.trim().split("  ").length <= 2 || !linha.trim().substring(0, cabecalho.length()).equalsIgnoreCase(cabecalho))) {
                                erros(linha, lineCount, "cabecalho de matriz de comparacao nao encontrado ,ou escrito incorretamente", outErros);
                                errorCount++;
                                outErros.close();
                                outAuxFile.close();
                                System.exit(0);
                            } else {
                                //se cabecalho correto, manda linha para ficheiro auxiliar
                                outAuxFile.format("%n%s", linha.trim());
                                //cria um vetor com cabecalho da matriz de comparacao
                                String[] vecAlt = linha.trim().split("  ");
                                /*
                                se for a primeira matriz de comparacao, vai estabelecer o seu numero de alternativas
                                como o numero a ser esperado pelas matrizes seguintes;
                                e verifica se as seguintes cumprem esse numero, retorna erro e sai do programa
                                 */
                                if (i == 1) {
                                    auxline = linha.trim().split("  ").length;
                                } else {
                                    if (vecAlt.length != auxline) {
                                        erros(linha, lineCount, "cabecalho de matriz de comparacao diferente do estabelecido na matriz anterior", outErros);
                                        errorCount++;
                                        outErros.close();
                                        outAuxFile.close();
                                        System.exit(0);

                                    }
                                }
                                linha = fInput.nextLine();
                                lineCount++;
                                /*
                                ciclo para correr pela matriz de alternaivas, segundo o numero de alternativas em vecAlt;
                                menos 1 porque:
                                vecCrit = ["mcp_criterio"][alternativa]...[alternativa]
                                funciona da mesma forma que o ciclo da matriz criterios
                                 */
                                for (int j = 0; j < vecAlt.length - 1; j++) {
                                    linha = linha.trim().replaceAll("\\s+", " ");
                                    if (linha.split(" ").length != vecAlt.length - 1) {
                                        erros(linha, lineCount, "numero de elementos da matriz incorreto", outErros);
                                        errorCount++;
                                    } else {
                                        String[] num = linha.split(" ");
                                        for (int x = 0; x < vecAlt.length - 1; x++) {
                                            if (auxiliar(num[x], linha, lineCount, outErros) == false) {
                                                erros(linha, lineCount, "valor numerico invalido", outErros);
                                                errorCount++;
                                            }
                                        }
                                        outAuxFile.format("%n%s", linha.trim());
                                    }
                                    if (fInput.hasNext()) {
                                        linha = fInput.nextLine();
                                        lineCount++;
                                    }
                                }
                            }
                        }
                        while (fInput.hasNext() && linha.trim().length() == 0) {
                            linha = fInput.nextLine();
                            lineCount++;
                        }
                    }
                }
            }

        }
        //numero de linhas igual a 0, ficheiro vazio
        if (lineCount == 0) {
            erros("", 0, "FICHEIRO VAZIO", outErros);
            errorCount++;
        }

        //se existriem erros sai do programa
        //senao apaga o ficheiro criado para erros 
        if (errorCount > 0) {
            outAuxFile.close();
            outErros.close();
            System.exit(0);

        } else {
            outAuxFile.close();
            outErros.close();
            File erros = new File(errorFile);
            erros.delete();
        }
    }

    /**
     * Metodo de verificaçao de integridade do ficheiro de leitura topsis,
     * retorna um String com os criterios
     *
     * @param fileName - ficheiro de entrada
     * @return
     * @throws FileNotFoundException
     */
    public static String errosTopsis(String fileName) throws FileNotFoundException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH'h'mm'm'ss's'");
        Date date = new Date();
        String errorFile = "erros_" + dateFormat.format(date);

        Scanner fInput = new Scanner(new File(fileName));
        //string onde vao ser introduzidos os criterios
        String criterios = "";
        //texto esperado nas descricoes
        String auxFile = "ficheiro_auxiliar";
        String auxB = "crt_beneficio";
        String auxC = "crt_custo";
        String auxP = "vec_pesos";
        String auxM = "md_crt_alt";
        String crt = "crt";
        String alt = "alt";
        String linha = fInput.nextLine();
        //ficheiro para onde vao ser passadas as linhas a ser tratadas, e ficheiro de erros
        Formatter outAuxFile = new Formatter(new File(auxFile)), outErros = new Formatter(new File(errorFile));
        int lineCount = 1, errorCount = 0, caso = 0, entradaCusto = 0, entradaBeneficio = 0;
        //vai correr pelo ficheiro de entrada
        while (fInput.hasNext()) {

            //ignora as linhas em branco
            while (fInput.hasNext() && linha.trim().length() == 0) {
                linha = fInput.nextLine();
                lineCount++;
            }

            //caso para entrar no switch, para tratamneto
            //linha menor que a menor descriçao esperada
            if (linha.trim().length() <= auxC.length()) {
                caso = 1;
            }
            //linha a começar com beneficios
            if (linha.trim().length() >= auxB.length() && linha.trim().substring(0, auxB.length()).trim().equalsIgnoreCase(auxB)) {
                caso = 2;
                entradaBeneficio++;
            }

            //linha a começar com pesos
            if (linha.trim().length() >= auxP.length() && linha.trim().substring(0, auxP.length()).trim().equalsIgnoreCase(auxP)) {
                caso = 3;
            }
            //linha a começar com custo
            //contador para auxiliar na validacao do ficheiro
            if (linha.trim().length() >= auxC.length() && linha.trim().substring(0, auxC.length()).trim().equalsIgnoreCase(auxC)) {
                caso = 4;
                entradaCusto++;
            }
            //linha a começar com md
            if (linha.trim().length() == auxM.length() && linha.trim().substring(0, auxM.length()).trim().equalsIgnoreCase(auxM)) {
                caso = 5;
            }
            /*
            verificaçao para falta de custo ou beneficio;
            se o ficheiro comecou com a descricao de custo, retorna erro e sai do programa
             */
            if ((caso == 3 || caso == 5) && criterios.length() == 0 && entradaCusto > 0) {
                erros(linha, lineCount, "descriçao de custo ou beneficio em falta(ambas sao necessarias)", outErros);
                errorCount++;
                outErros.close();
                outAuxFile.close();
                System.exit(0);
            }

            //casos para tratamento; seguir casos estabelecidos em cima
            switch (caso) {
                case 1:
                    erros(linha, lineCount, "nenhuma descriçao encontrada  ou linha com informaçao desnecessaria", outErros);
                    errorCount++;
                    outErros.close();
                    outAuxFile.close();
                    System.exit(0);
                    break;
                case 2:
                    //econtrou beneficios; comeca a montar os criterios
                    criterios = linha.trim().substring(auxB.length()).trim();
                    criterios = criterios.trim();

                    break;
                case 3:
                    //caso nao existam beneficios e custos, nao existem criterios; vai retirar os criterios do cabecalho de pesos
                    //monta vetor de pesos
                    if (criterios.length() == 0) {
                        criterios = linha.trim().substring(auxP.length()).trim();
                        criterios = criterios.trim();
                        linha = fInput.nextLine();
                        lineCount++;

                        linha = linha.trim().replaceAll("\\s+", " ");
                        //??verificacao redundante??alterar
                        if (linha.substring(auxP.length()).trim().equalsIgnoreCase(criterios.trim())) {
                            erros(linha, lineCount, "Numero de elementos do vetor de pesos nao corresponde ao numero de criterios", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        } else {
                            String[] num = linha.split(" ");
                            for (int j = 0; j < criterios.split("  ").length - 1; j++) {
                                if (auxiliar(num[j], linha, lineCount, outErros) == false) {
                                    erros(linha, lineCount, "valor numerico invalido", outErros);
                                    errorCount++;
                                }
                            }
                            outAuxFile.format("%s%n", linha);
                        }
                    } else {
                        //caso existam custos e beneficios;verifica se a a linha tem os criterios corretos
                        //monta vetor criterios
                        if (!linha.trim().substring(auxP.length()).trim().equalsIgnoreCase(criterios.trim())) {
                            erros(linha, lineCount, "Criterios do vetor pesos, estao incorretos", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        }
                        linha = fInput.nextLine();
                        lineCount++;
                        linha = linha.trim().replaceAll("\\s+", " ");
                        //??verificacao errada e desnecessaria??
                        if (linha.substring(auxP.length()).trim().equalsIgnoreCase(criterios.trim())) {
                            erros(linha, lineCount, "Criterios do vetor pesos, estao incorretos", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        } else {
                            String[] num = linha.trim().replaceAll("\\s+", " ").split(" ");
                            if (num.length != criterios.trim().split("  ").length) {
                                erros(linha, lineCount, "numero de elementos da vetor incorreto", outErros);
                                errorCount++;
                                outErros.close();
                                outAuxFile.close();
                                System.exit(0);
                            }
                            for (int j = 0; j < criterios.trim().split("  ").length; j++) {
                                if (auxiliar(num[j], linha, lineCount, outErros) == false) {
                                    erros(linha, lineCount, "valor numerico invalido", outErros);
                                    errorCount++;
                                }
                            }
                            outAuxFile.format("%s%n", linha);
                        }
                    }
                    //se existir vetor de pesos, escreve 1 na matriz principal
                    theMatrix[0][1][0] = 1;
                    break;
                case 4:
                    /*
                    encontrou custos,mas se cnao encontrou beneficios antes
                    retorna erro
                    termina programa
                     */
                    if (entradaBeneficio == 0) {
                        erros(linha, lineCount, "descriçao de custo ou beneficio em falta(ambas sao necessarias)", outErros);
                        errorCount++;
                        outErros.close();
                        outAuxFile.close();
                        System.exit(0);
                    } else {
                        //escreve quantos custos existem na matriz principal
                        theMatrix[0][0][0] = linha.trim().split("  ").length - 1;
                        //adiciona criterios em custos aos criterios
                        criterios = criterios + "  " + linha.trim().substring(auxC.length()).trim();
                    }
                    break;
                case 5:
                    linha = fInput.nextLine();
                    lineCount++;
                    /* se nao existirem criterios estabelecidos, ou seja,
                    falta de todas as outras descricoes;beneficios/custos e pesos
                    estabelece criterios
                    estabelece numero de alternativas
                     */
                    if (criterios.length() == 0) {
                        //verifica se descricao de criterios esta correta
                        if (linha.trim().length() < crt.length() || (linha.trim().length() >= crt.length() && !linha.trim().substring(0, crt.length()).equalsIgnoreCase(crt))) {
                            erros(linha, lineCount, "descriçao criterios incorreta ou inexistente", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        }
                        criterios = linha.trim().replaceAll("\\s+", "  ").substring(crt.length()).trim();
                        linha = fInput.nextLine();
                        lineCount++;
                        //verifica se descricao de alternativas esta correta
                        if (linha.trim().length() < alt.length() || (linha.trim().length() >= alt.length() && !linha.trim().substring(0, alt.length()).equalsIgnoreCase(alt))) {
                            erros(linha, lineCount, "descriçao alternativas incorreta ou inexistente", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        }
                        int nAlt = linha.trim().replaceAll("\\s+", "  ").substring(alt.length()).trim().split("  ").length;
                        linha = fInput.nextLine();
                        lineCount++;
                        //correr pela matriz consoante o nAlt e nCrit
                        for (int j = 0; j < nAlt; j++) {
                            linha = linha.trim().replaceAll("\\s+", " ");

                            if (linha.split(" ").length != criterios.trim().split("  ").length) {
                                erros(linha, lineCount, "numero de elementos da matriz incorreto", outErros);
                                errorCount++;
                                outErros.close();
                                outAuxFile.close();
                                System.exit(0);
                            } else {
                                String[] num = linha.split(" ");

                                for (int x = 0; x < criterios.trim().split("  ").length; x++) {
                                    if (auxiliar(num[x], linha, lineCount, outErros) == false) {
                                        erros(linha, lineCount, "valor numerico invalido", outErros);
                                        errorCount++;
                                    }
                                }
                                outAuxFile.format("%s%n", linha.trim());
                            }
                            if (fInput.hasNext()) {
                                linha = fInput.nextLine();
                                lineCount++;
                            }
                        }
                    } else {
                        /*
                        se existriem criterios estabelecidos
                        validacoes da linha de crt e alt
                        corre a matriz e valida
                         */
                        if (linha.trim().length() < crt.length() || (linha.trim().length() >= crt.length() && !linha.trim().substring(0, crt.length()).equalsIgnoreCase(crt))) {
                            erros(linha, lineCount, "descriçao criterios incorreta ou inexistente", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        }
                        if (!linha.trim().substring(crt.length()).trim().equalsIgnoreCase(criterios.trim())) {
                            erros(linha, lineCount, "criterios incorretos", outErros);
                            errorCount++;
                            outErros.close();
                            outAuxFile.close();
                            System.exit(0);
                        } else {
                            linha = fInput.nextLine();
                            lineCount++;
                            if (linha.trim().length() < alt.length() || (linha.trim().length() >= alt.length() && !linha.trim().substring(0, alt.length()).equalsIgnoreCase(alt))) {
                                erros(linha, lineCount, "descriçao alternativas incorreta ou inexistente", outErros);
                                errorCount++;
                                outErros.close();
                                outAuxFile.close();
                                System.exit(0);
                            }
                            if ((linha.trim().length() <= alt.length()) || (linha.trim().length() > alt.length() && !linha.trim().substring(0, alt.length()).trim().equalsIgnoreCase(alt))) {
                                erros(linha, lineCount, "alternativas incorretos", outErros);
                                errorCount++;
                                outErros.close();
                                outAuxFile.close();
                                System.exit(0);
                            } else {

                                int nAlt = linha.trim().substring(alt.length()).trim().split("  ").length;
                                linha = fInput.nextLine();
                                lineCount++;

                                for (int j = 0; j < nAlt; j++) {
                                    linha = linha.trim().replaceAll("\\s+", " ");

                                    if (linha.split(" ").length != criterios.trim().split("  ").length) {
                                        erros(linha, lineCount, "numero de elementos da matriz incorreto", outErros);
                                        errorCount++;
                                    } else {
                                        String[] num = linha.split(" ");
                                        for (int x = 0; x < criterios.trim().split("  ").length; x++) {
                                            if (auxiliar(num[x], linha, lineCount, outErros) == false) {
                                                erros(linha, lineCount, "valor numerico invalido", outErros);
                                                errorCount++;
                                            }
                                        }
                                        outAuxFile.format("%s%n", linha.trim());
                                    }
                                    if (fInput.hasNext() && j < nAlt - 1) {
                                        linha = fInput.nextLine();
                                        lineCount++;
                                    } else {
                                        if (!fInput.hasNext() && j < nAlt - 1) {
                                            erros(linha, lineCount, "numero de elementos da matriz incorreto", outErros);
                                            errorCount++;
                                            break;
                                        }
                                    }

                                }

                            }
                        }
                    }
                    break;
                // se nao se der nenhuma das ocorrencias anteriores
                case 0:
                    erros("", 0, "ficheiro sem informaçao legivel, ou vazio", outErros);
                    errorCount++;
                    outErros.close();
                    outAuxFile.close();
                    System.exit(0);
            }
            //verificacao , para caso o ficheiro continua depois de correr a ultima matriz
            if (fInput.hasNext() && caso != 5) {
                linha = fInput.nextLine();
                lineCount++;
            } else {
                if (fInput.hasNext() && caso == 5) {
                    linha = fInput.nextLine();
                    lineCount++;
                    erros(linha, lineCount, "nenhuma descriçao encontrada  ou linha com informaçao desnecessaria", outErros);
                    errorCount++;
                    outErros.close();
                    outAuxFile.close();
                    System.exit(0);
                }
            }
            caso = 0;

        }
        
        //se existriem erros sai do programa
        //senao apaga o ficheiro criado para erros 
        if (errorCount > 0) {
            outAuxFile.close();
            outErros.close();
            System.exit(0);

        } else {
            outAuxFile.close();
            outErros.close();
            File erros = new File(errorFile);
            erros.delete();
        }
        return criterios.trim();
    }

    /**
     * converte o se necessario o numero com barra, corre pelos chars do string
     *
     * @param num - String com um suposto numero
     * @param linha - linha onde se encontra o numeros
     * @param lineCount - contador de linhas
     * @param out - saida para ficheiro de erros
     * @return
     * @throws FileNotFoundException
     */
    public static boolean auxiliar(String num, String linha, int lineCount, Formatter out) throws FileNotFoundException {
        String barra = "/";

        //procura no string numero uma barra
        for (int i = 0; i < num.trim().length(); i++) {
            //se existir uma barra, faz split ao numero pela barra
            if (num.trim().charAt(i) == barra.charAt(0)) {
                String[] aux = num.split(barra);
                //se o split pela "/" tiver mais que 2 elementos, false
                if (aux.length != 2) {
                    erros(linha, lineCount, "valor numerico invalido", out);
                    return false;
                } else {
                    //verifica se os Strings dividios pela barra sao numeros validos
                    if (isNumericString(aux[0]) == false || isNumericString(aux[1]) == false) {
                        return false;
                    }
                }

                return true;
            }
        }
        //se nao existirem barra, verifica se o numero é validos
        return isNumericString(num.trim());
    }

    /**
     * verificaca se e numero
     *
     * @param num - String com numero a ser testado
     * @return
     */
    public static boolean isNumericString(String num) {
        boolean result = false;
        int count = 0;

        if (num != null && num.length() > 0) {
            char[] charArray = num.toCharArray();
            for (int i = 0; i < charArray.length; i++) {
                if (charArray[i] >= '0' && charArray[i] <= '9' || charArray[i] == '.') {
                    if (charArray[i] == '.') {
                        count++;
                    } else {
                        if (!Character.isDigit(charArray[i])) {
                            count++;
                        }
                    }
                    if (count > 1) {
                        result = false;
                        break;
                    }
                    result = true;
                } else {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * output de erros
     *
     * @param linha - linha do ficheiro com erro
     * @param numeroLinha
     * @param txt - msg a ser mostrada
     * @param out - ficheiro de saida
     * @throws FileNotFoundException
     */
    public static void erros(String linha, int numeroLinha, String txt, Formatter out) throws FileNotFoundException {

        out.format("%S%n%S%n%s%n%s%d%n", txt, "linha com erro:", linha, "Numero da linha= ", numeroLinha);

        System.out.printf("%S%n%S%n%s%n%s%d%n", txt, "linha com erro:", linha, "Numero da linha= ", numeroLinha);

    }

}
