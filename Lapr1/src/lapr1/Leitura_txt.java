package lapr1;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import static lapr1.Lapr1.theMatrix;

/*
    !!!!!!!!!!preenchimento da matriz(slots/gavetas)!!!!!!!!!!!
    matrizes do ficheiro:   [0-(nCrit+1)[
    matrizes normalizadas:   [(nCrit+1)-((nCrit +1)*2)[
    vetores proprios:   [((nCrit +1)*2)-((nCrit +1)*3)[
    valores pproprios(lambda max):   [((nCrit +1)*3)]
    rc:    [(((nCrit +1)*3)+1)]
    vetor composto:       [(((nCrit +1)*3)+2)]
    escolha:   [(((nCrit +1)*3)+3)]
 */
public class Leitura_txt {

    /**
     * retorna o cabecalho de criterios e introduz a matriz na matriz principal
     *
     * @return
     * @throws FileNotFoundException
     */
    public static String[] lerNumCriterios() throws FileNotFoundException {
        //ficheiro onde se encontra a informacao 
        Scanner fInput = new Scanner(new File("ficheiro_auxiliar"));
        String linha = fInput.nextLine();
        String[] vecCrit = linha.split("  ");
        int nCrit = vecCrit.length - 1;
        linha = fInput.nextLine();

        for (int i = 0; i < nCrit; i++) {
            //subsitui espa�os repetidos por um espa�o
            linha = linha.trim().replaceAll("\\s+", " ");

            String[] num = linha.split(" ");

            for (int j = 0; j < nCrit; j++) {
                //metodo auxilar para converter as fracoes
                theMatrix[i][j][0] = auxiliar(num[j]);
            }
            linha = fInput.nextLine();
        }

        return vecCrit;
    }

    /**
     * retorna o cabecalho de alternativas e introduz matrizes de comparacao na
     * matriz principal
     *
     * @param nCrit - numero de criterios
     * @return
     * @throws FileNotFoundException
     */
    public static String[] lerNumAlternativas(int nCrit) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File("ficheiro_auxiliar"));
        String linha = fInput.nextLine();
        for (int i = 0; i < nCrit + 1; i++) {
            linha = fInput.nextLine();
        }
        String[] vecAlt = linha.split("  ");

        for (int i = 1; i < nCrit + 1; i++) {

            for (int j = 0; j < vecAlt.length - 1; j++) {
                linha = fInput.nextLine();
                String[] num = linha.split(" ");
                for (int k = 0; k < vecAlt.length - 1; k++) {
                    theMatrix[j][k][i] = auxiliar(num[k]);
                }

            }
            //para ignorar cabecalho seguinte
            if (fInput.hasNext()) {
                linha = fInput.nextLine();
            }
        }
        return vecAlt;
    }

    /**
     * converte o se necessario o numero com barra
     *
     * @param num - string com numero
     * @return - retorna o numero convertido
     * @throws FileNotFoundException
     */
    public static double auxiliar(String num) throws FileNotFoundException {
        String barra = "/";
        for (int i = 0; i < num.trim().length(); i++) {
            if (num.trim().charAt(i) == barra.charAt(0)) {
                String[] aux = num.split(barra);
                double n = (Double.valueOf(aux[0].trim()) / (Double.valueOf(aux[1].trim())));
                return n;
            }
        }
        double n = Double.valueOf(num.trim());
        return n;
    }

    /**
     * metodo para introduzir vetores calculadas na matriz principal
     *
     * @param theMatrix - matriz principal
     * @param slot - posicao na matriz (z), consultar legenda no topo
     * @param vetor - vetor a ser introduzido
     * @param num - numero total de linhas
     * @param num2 - coluna
     */
    public static void introduzirVetoresUnicos(double[][][] theMatrix, int slot, double[] vetor, int num, int num2) {
        for (int i = 0; i < num; i++) {

            theMatrix[i][num2][slot] = vetor[i];

        }
    }

    /**
     * metodo para introduzir matrizes na matriz principal
     *
     * @param theMatrix - matriz principal
     * @param slot - posicao na matriz (z), consultar legenda no topo
     * @param matriz - matriz a ser introduzido
     * @param num - numero total de linhas
     * @param num2 - numero total de colunas
     */
    public static void introduzirMatrizesCalc(double[][][] theMatrix, int slot, double[][] matriz, int num, int num2) {
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num2; j++) {
                theMatrix[i][j][slot] = matriz[i][j];

            }

        }
    }

    //leitura topsis
    /**
     * vai buscar ao ficheiro de entrada um vetor com as alternativas
     *
     * @param fileName - ficheiro e entrada
     * @param crit - String com os criterios
     * @return - vetor de alternativas
     * @throws FileNotFoundException
     */
    public static String[] vetorAlternativasTopsis(String fileName, String crit) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(fileName));
        String[] aux = {""};
        String linha = fInput.nextLine();

        while (fInput.hasNext()) {
            linha = fInput.nextLine();
            if (linha.trim().length() >= crit.length() + 3 && linha.trim().replaceAll("\\s+", "  ").substring(3).trim().equalsIgnoreCase(crit)) {
                linha = fInput.nextLine();

                return linha.trim().replaceAll("\\s+", "  ").substring(4).trim().split("  ");

            }

        }
        return aux;
    }

    /**
     * vai percorrer o ficheiro auxiliar para introduzir vetor de pesos e matriz
     * de decisao
     *
     * @param vecCrit - vetor de criterios
     * @param vecAlt - vetor de alternativas
     * @throws FileNotFoundException
     */
    public static void leituraTopsis(String[] vecCrit, String[] vecAlt) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File("ficheiro_auxiliar"));
        String linha = fInput.nextLine();
        //se existriem pesos definidos pelo user, introduz na matriz principal
        if (theMatrix[0][1][0] == 1) {
            String[] pesos = linha.trim().split(" ");
            for (int i = 0; i < vecCrit.length; i++) {
                theMatrix[i][0][0] = auxiliar(pesos[i]);
            }
            linha = fInput.nextLine();
        }

        //introduz matriz de decisao na matriz principal
        for (int j = 0; j < vecAlt.length; j++) {

            String[] num = linha.split(" ");
            for (int k = 0; k < vecCrit.length; k++) {
                theMatrix[j][k][1] = auxiliar(num[k]);
            }
            if (fInput.hasNext()) {
                linha = fInput.nextLine();
            }
        }
    }

    /**
     * verica numero é valido
     *
     * @param num - string numero
     * @return
     */
    public static boolean isNumericString(String num) {
        boolean result = false;
        int count = 0;

        if (num != null && num.length() > 0) {
            char[] charArray = num.toCharArray();

            for (int i = 0; i < charArray.length; i++) {

                if (charArray[i] >= '0' && charArray[i] <= '9' || charArray[i] == '.') {
                    if (charArray[i] == '.') {
                        count++;
                    }
                    if (count > 1) {
                        result = false;
                        break;
                    }
                    result = true;
                } else {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

}
