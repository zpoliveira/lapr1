package lapr1;

public class LIMIAR {

    public static int verificaLimiar(String limiar, double[][][] theMatrix, int nCrit, String[] crit, String[] critEliminados) {

        double limiar_D = Double.parseDouble(limiar);

        double[][] matCrit = MetodosComuns.criaMatAux(theMatrix, nCrit, nCrit, 0);

        matCrit = MetodosComuns.matrizNormalizada(matCrit, nCrit);

        double[] vecPropCriterios = CalcAHP.VetorProprioMan(matCrit, nCrit);

        int indice = 0, z = 0;

        do {

            if (vecPropCriterios[indice] < limiar_D) {

                int critElim = indice;

                critEliminados[z + 1] = crit[critElim + 1];
                z++;

                EliminaValoresVetor(vecPropCriterios, critElim, nCrit);

                EditaCabecalho(crit, critElim, nCrit);

                EditaMatrizCriterios(theMatrix, critElim, nCrit);

                nCrit = nCrit - 1;

                indice = 0;
                //RECALCULA O ARRAY DE PRIORIDADES, NO CASO DE ALGUM CRITÉRIO SER EXCLUIDO
                matCrit = MetodosComuns.criaMatAux(theMatrix, nCrit, nCrit, 0);
                matCrit = MetodosComuns.matrizNormalizada(matCrit, nCrit);
                vecPropCriterios = CalcAHP.VetorProprioMan(matCrit, nCrit);

            } else {
                indice++;
            }

        } while (indice != nCrit);

        String el = String.valueOf(z);
        critEliminados[0] = el;

        //VERIFICA SE O NUMERO DE CRITERIOS É >2 PARA QUE SE POSSA PROSSEGUIR COM O CALCULO.
        if (nCrit <= 2) {
            if(z==0) {
            System.out.println("\n" + "Número de critérios de entrada insuficientes (devem ser mais de 2).");
            System.out.println("Assim não é possivel o calculo matemático exato.");
            System.exit(0);
            } else {
                System.out.println("\n" + "Foram eliminados " + critEliminados[0] + " critérios, de acordo com o limiar definido (" + limiar + ")");
            System.out.println("Assim não é possivel o calculo matemático exato.");
            System.exit(0);
            }
        }

        return nCrit;
    }

    //ELIMINA UM DETERMINADO VALOR (critElim) DUM VETOR
    public static void EliminaValoresVetor(double[] vecPropCriterios, int critElim, int nCrit) {

        for (int j = critElim; j < nCrit - 1; j++) {
            vecPropCriterios[j] = vecPropCriterios[j + 1];
        }
    }

    //EDITA O CABECALHO DOS CRITERIOS DE ACORDO COM OS AJUSTES DO MÉTODO
    //ELiminaValoresVetor
    public static void EditaCabecalho(String[] cabecalho, int critElim, int nCrit) {

        for (int j = critElim + 1; j < nCrit; j++) {
            cabecalho[j] = cabecalho[j + 1];
        }
    }

    public static void EditaMatrizCriterios(double[][][] theMatrix, int critElim, int nCrit) {

        for (int i = critElim; i < nCrit; i++) {
            //   if(i==critElim) {
            for (int j = 0; j < nCrit; j++) {
                theMatrix[j][i][0] = theMatrix[j][i + 1][0];
            }
            for (int j = 0; j < nCrit; j++) {
                theMatrix[i][j][0] = theMatrix[i + 1][j][0];
            }
            //}
        }
    }

}
