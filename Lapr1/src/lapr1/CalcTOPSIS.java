package lapr1;

public class CalcTOPSIS {

    /*   
    !!!!!!!!!!preenchimento da matriz(slots/gavetas)TOPSIS!!!!!!!!!!!
    vetor de pesos:   [0]
    matriz decisao:   [1]
    matriz decisao normalizada:   [2]
    matriz decisao normalizada pesada:   [3]
    solucoes ideais:    [4] 2 vetores
    distancia alternativas:       [5] 2 vetores
    vetor proximidades:   [6] 1 vetor
    alternativa escolhida: [7]
     */

// metodo para calculo da matriz normalizada segundo metodologia TOPSIS
    // recebe uma matriz e a respetiva dimensao
    /**
     *
     * @param matriz - Matriz de decisão
     * @param criterios - Inteiro com número de critérios
     * @param alternativas - Inteiro com número de alternativas
     * @return - Matriz normalizada
     */
    public static double[][] normalizador(double[][] matriz, int alternativas, int criterios) {
        double[][] normalizada = new double[alternativas][criterios];

        // para facilitar, primeiro calcula se a raiz da soma dos elementos da coluna
        double somaquad = 0;
        double[] raiz = new double[criterios];

        for (int j = 0; j < criterios; j++) {
            for (int i = 0; i < alternativas; i++) {

                somaquad = somaquad + (matriz[i][j] * matriz[i][j]);

            }
            raiz[j] = Math.sqrt(somaquad);

            somaquad = 0;
        }

        // segunda fase - Divide-se o elemento da matriz pela raiz dos quadrados
        for (int j = 0; j < criterios; j++) {
            for (int i = 0; i < alternativas; i++) {
                normalizada[i][j] = matriz[i][j] / raiz[j];

            }

        }

        return normalizada;

    }

    //Cálculo de matriz pesada
    /**
     *
     * @param matriz - Matriz normalizada
     * @param custo - Inteiro com número de parâmetros de custos
     * @param criterios - Inteiro com número de critérios
     * @param alternativas - Inteiro com número de alternativas
     * @return - Matriz normalizada pesada
     */
    public static double[][] matriznormpesada(double[][] matriz, double[] pesos, int criterios, int alternativas) {

        double[][] mnp = new double[alternativas][criterios];

        for (int j = 0; j < criterios; j++) {
            for (int i = 0; i < alternativas; i++) {
                mnp[i][j] = matriz[i][j] * pesos[j];

            }

        }
        return mnp;
    }

    //Cálculo de vetor ideal positivo
    /**
     *
     * @param matriz - Matriz normalizada pesada
     * @param criterios - Inteiro com número de critérios
     * @param alternativas - Inteiro com número de alternativas
     * @param custo - Inteiro com número de parâmetros de custos
     * @return - Vetor da solução ideal positiva
     */
    public static double[] idealpositivo(double[][] matriz, int criterios, int alternativas, int custo) {
        double[] idealmax = new double[criterios];
        int beneficios = criterios - custo;
        int indice = 0;

        for (indice = 0; indice < beneficios; indice++) {
            idealmax[indice] = 0;
            for (int i = 0; i < alternativas; i++) {
                if (matriz[i][indice] > idealmax[indice]) {
                    idealmax[indice] = matriz[i][indice];
                }

            }

        }

        while (indice != criterios) {
            idealmax[indice] = 1;
            for (int i = 0; i < alternativas; i++) {
                if (matriz[i][indice] < idealmax[indice]) {
                    idealmax[indice] = matriz[i][indice];
                }

            }

            indice++;

        }

        return idealmax;
    }

    //Cálculo de vetor ideal negativo
    /**
     *
     * @param matriz - Matriz normalizada pesada
     * @param criterios - Inteiro com número de critérios
     * @param alternativas - Inteiro com número de alternativas
     * @param custo - Inteiro com número de parâmetros de custos
     * @return - Vetor da solução ideal negativa
     */
    public static double[] idealnegativo(double[][] matriz, int criterios, int alternativas, int custo) {
        double[] idealmin = new double[criterios];
        int beneficios = criterios - custo;
        int indice = 0;

        for (indice = 0; indice < beneficios; indice++) {
            idealmin[indice] = 1;
            for (int i = 0; i < alternativas; i++) {
                if (matriz[i][indice] < idealmin[indice]) {
                    idealmin[indice] = matriz[i][indice];
                }

            }

        }
        while (indice != criterios) {
            idealmin[indice] = 0;
            for (int i = 0; i < alternativas; i++) {
                if (matriz[i][indice] > idealmin[indice]) {
                    idealmin[indice] = matriz[i][indice];
                }

            }

            indice++;

        }
        return idealmin;
    }

    //Cálculo de distância euclidiana (para ambos os casos)
    /**
     *
     * @param matriz - Matriz normalizada pesada
     * @param criterios - Inteiro com o número de critérios
     * @param alternativas - Inteiro com o número de alternativas
     * @param ideal - Vector da solução ideal (positiva ou negativa)
     * @return - Vetor de distância euclidiana relativamente à solução escolhida
     * (positiva ou negativa)
     */
    public static double[] distanciaalternativa(double[][] matriz, int alternativas, int criterios, double[] ideal) {
        double[] distancia = new double[alternativas];
        double somaquad = 0;

        for (int i = 0; i < alternativas; i++) {
            for (int j = 0; j < criterios; j++) {

                somaquad = somaquad + Math.pow(ideal[j] - matriz[i][j], 2);
            }
            distancia[i] = Math.sqrt(somaquad);

            somaquad = 0;
        }
        return distancia;
    }

    //Cálculo de vetor proximidade
    /**
     *
     * @param distanciapositiva - Vetor de distância à solução ideal positiva
     * @param distancianegativa - Vetor de distância à solução ideal negativa
     * @param alternativas - Inteiro com número de alternativas
     * @return - Vetor de ordenamento de opções em função da distância quer à
     * solução ideal positiva, quer à negativa
     */
    public static double[] proximidade(double[] distanciapositiva, double[] distancianegativa, int alternativas) {
        double[] proximidade = new double[alternativas];

        for (int i = 0; i < alternativas; i++) {
            proximidade[i] = distancianegativa[i] / (distancianegativa[i] + distanciapositiva[i]);

        }
        return proximidade;
    }

}
